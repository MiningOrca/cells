import cells.SimpleCell;
import feild.Field;
import logic.GameLogic;
import model.Coords;
import object.lamp.SimpleLamp;
import org.junit.Test;

public class FieldTest {

    Field field = new Field(5, 5);

    @Test
    public void testField() {
        field.printField();
        System.out.println();
        field.initCell(new SimpleCell(), new Coords(4, 4));
        field.printField();
        System.out.println();
        field.getFieldCells()[4][4].getCell().printVisionField();
    }

    @Test
    public void TestGame() {
        new GameLogic()
                .initField(30, 50)
                .setStuff(new SimpleLamp(5, new Coords(15, 25)))
                .initSimpleCell(15, 25)
                .evolveField(300);
    }
}
