package model;


public class MemoryUnit {

    double moveWeight;
    double splitWeight;
    int lifeTime;
    int generation;
    boolean isPathCell;

    public MemoryUnit(int generation) {
        this.generation = generation;
    }

    public boolean isPathCell(){
        return isPathCell;
    }

    public double getMoveWeight() {
        return moveWeight;
    }

    public void setMoveWeight(double moveWeight) {
        this.moveWeight = this.moveWeight + moveWeight;
    }

    public double getSplitWeight() {
        return splitWeight;
    }

    public void setSplitWeight(double splitWeight) {
        this.splitWeight = this.splitWeight + splitWeight;
    }

    public boolean isDieTime(int generation) {
        return this.generation + lifeTime == generation;
    }
}
