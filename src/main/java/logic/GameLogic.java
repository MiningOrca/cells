package logic;

import cells.SimpleCell;
import feild.Field;
import feild.FieldCell;
import model.Coords;
import object.lamp.IStuff;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public class GameLogic {
    private Field field;


    public GameLogic initField(int longitude, int latitude) {
        this.field = new Field(longitude, latitude);
        return this;
    }

    public static void printFullFieldStat(Field field) {
        StringBuilder Cells = new StringBuilder("Cells\n");
        StringBuilder Light = new StringBuilder("Light level\n");
        for (FieldCell[] coord : field.getFieldCells()) {
            for (FieldCell cell : coord) {
                if (cell != null) Cells.append(cell.getCell() != null ? cell.getCell() : "-").append(" ");
                else Cells.append("/ ");
                if (cell != null) Light.append(cell.getLightLevel()).append(" ");
                else Light.append("NaN ");
            }
            Cells.append("\n");
            Light.append("\n");
        }
        System.out.println(Cells.toString());
        System.out.println(Light.toString());
    }

    public GameLogic initSimpleCell(int x, int y) {
        if (x < 1 || y < 1) throw new IllegalArgumentException("Coords start with 1");
        if (field != null) {
            field.initCell(new SimpleCell(), new Coords(x - 1, y - 1));
        } else {
            throw new InvalidParameterException("Should init field first");
        }
        return this;
    }

    public GameLogic setStuff(IStuff stuff) {
        field.setStaff(stuff);
        return this;
    }

    public GameLogic evolveField(int steps) {
        int i = 0;
        while (i < steps) {
            Stream<SimpleCell> simpleCellStream = Arrays.stream(field.getFieldCells()).flatMap((fieldCells -> Arrays.stream(fieldCells).map(FieldCell::getCell)));
            simpleCellStream.filter(Objects::nonNull).forEach(SimpleCell::evolve);
            System.out.println("Step No " + i);
//            printFullFieldStat();
            field.printField();
            i++;
        }
        return this;
    }

}
