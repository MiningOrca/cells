package object.lamp;

import feild.Field;
import feild.FieldCell;
import model.Coords;

public class SimpleLamp implements IStuff {
    private Coords lampPosition;
    private Field lightField;
    private int radius;
    private int diameter;
    private double lampLight;
    private double lampStep = 0.5;

    public SimpleLamp(int radius, Coords coords) {
        this.radius = radius;
        this.lampPosition = coords;
        this.diameter = 1 + radius * 2;
    }

    public SimpleLamp(double lumpPower, Coords coords) {
        //todo will be finish soon. May be
    }

    public void activate(Field field) {
        FieldCell centerCell = field.getFieldCell(lampPosition);
        this.lightField = centerCell.trimWalls(radius);
        for (int x = 0; x <= radius; x++) {
            lampLight = lampStep * (x + 1);
            setLightLevel(new Coords(x, x));
            if (x != radius) { //ony one can set light in the center
                setLightLevel(new Coords(diameter - 1 - x, diameter - 1 - x));
                setLightLevel(new Coords(diameter - 1 - x, x));
                setLightLevel(new Coords(x, diameter - 1 - x));
            }
            for (int y = 0; y < diameter; y++) {
                if (y != x && x != radius && y != diameter - 1 - x) {//diagonal axis and center light and some more diagonal axis
                    if (y < diameter - 1 - x) { //can't fill under diagonal
                        setLightLevel(new Coords(x, y));
                        setLightLevel(new Coords(y, x));
                    }
                    if (x < diameter - y && y > x) { //can't fill diagonal and all upper
                        setLightLevel(new Coords(diameter - 1 - x, y));
                        setLightLevel(new Coords(y, diameter - 1 - x));
                    }
                }
            }
        }
    }

    private void setLightLevel(Coords coords) {
        FieldCell cell = lightField.getFieldCell(coords);
        if (cell != null) cell.setLightLevel(lampLight);
    }
}