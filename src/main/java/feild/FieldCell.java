package feild;

import cells.SimpleCell;
import model.Coords;

public class FieldCell {
    private Field field;
    private SimpleCell cell;
    private Coords coords;
    private double lightLevel;

    public FieldCell(Field field, Coords coords) {
        this.field = field;
        this.coords = coords;
    }

    public Coords getCoords() {
        return coords;
    }

    public SimpleCell getCell() {
        return cell;
    }

    public void setCell(SimpleCell cell) {
        this.cell = cell;
        cell.setFieldCell(this);
        cell.setVisionField(visionField(cell.getVisionRadius()));
    }

    public void killCell() {
        this.cell = null;
    }

    public FieldCell getUpperCell() {
        return coords.getY() > 0 ? field.getFieldCells()[coords.getX()][coords.getY() - 1] : null;
    }

    public FieldCell getBottomCell() {
        return coords.getY() < field.getFieldCells()[0].length ? field.getFieldCells()[coords.getX()][coords.getY() + 1] : null;
    }

    public FieldCell getLeftCell() {
        return coords.getX() > 0 ? field.getFieldCells()[coords.getX() - 1][coords.getY()] : null;
    }

    public FieldCell getRightCell() {
        return coords.getX() < field.getFieldCells().length ? field.getFieldCells()[coords.getX() + 1][coords.getY()] : null;
    }

    public Field visionField(int radius) {
        Field visionField = trimWalls(radius);
        visionField.getFieldCells()[radius][radius] = null;
        return visionField;
    }

    public Field trimWalls(int radius) {
        int diameter = 1 + radius * 2;
        Field visionField = new Field(diameter, diameter);
        for (int x = 0; x < diameter; x++) {
            for (int y = 0; y < diameter; y++) {
                Coords realCoord = getRealCoord(new Coords(x, y), radius);
                visionField.getFieldCells()[x][y] = isWall(realCoord) ? null : field.getFieldCells()[realCoord.getX()][realCoord.getY()];
            }
        }
        return visionField;
    }

    private Coords getRealCoord(Coords internalCoord, int radius) {
        int realX = internalCoord.getX() == radius ? coords.getX() : coords.getX() + internalCoord.getX() - radius;
        int realY = internalCoord.getY() == radius ? coords.getY() : coords.getY() + internalCoord.getY() - radius;
        return new Coords(realX, realY);
    }

    private boolean isWall(Coords coords) {
        return coords.getY() < 0 ||
                coords.getY() >= field.getFieldCells()[0].length ||
                coords.getX() < 0 ||
                coords.getX() >= field.getFieldCells().length;
    }

    public double getLightLevel() {
        return lightLevel;
    }

    public void setLightLevel(double lightLevel) {
        //we should just sum light level it's simple logic
        this.lightLevel = this.lightLevel + lightLevel;
        if (this.lightLevel < 0) this.lightLevel = 0;
    }
}
