package feild;

import cells.SimpleCell;
import model.Coords;
import object.lamp.IStuff;


public class Field {
    private FieldCell[][] fieldCells;

    public Field(int x, int y) {
        fieldCells = new FieldCell[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                fieldCells[i][j] = new FieldCell(this, new Coords(i, j));
            }
        }
    }

    public void initCell(SimpleCell cell, Coords coords) {
        fieldCells[coords.getX()][coords.getY()].setCell(cell);
    }

    public FieldCell[][] getFieldCells() {
        return fieldCells;
    }

    public void setStaff(IStuff staff) {
        staff.activate(this);
    }

    public FieldCell getFieldCell(Coords coords) {
        return fieldCells[coords.getX()][coords.getY()];
    }

    public void printField() {
        for (FieldCell[] coord : fieldCells) {
            for (FieldCell cell : coord) {
                System.out.print(cell.getCell() != null ? cell.getCell() + " " : "- ");
            }
            System.out.println();
        }
    }
}
