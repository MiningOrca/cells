package cells;

import feild.Field;
import feild.FieldCell;

//is it ever necessary?
public class ToxinCell extends SimpleCell {

    public ToxinCell() {
        this.lifetime = 1;
        this.generation = 0;
        this.visionRadius = 1;
        this.energy = 0;
    }

    @Override
    public void evolve() {
        generation++;
        if (lifetime == generation) die();
    }

    @Override
    protected void die() {
        Field poisonField = fieldCell.visionField(visionRadius);
        roundAction(poisonField, this::killThemAll);
        fieldCell.killCell();
    }

    private void killThemAll(FieldCell targetCell) {
        if (targetCell.getCell() != null) {
            if (!(targetCell.getCell().getClass().equals(DeadCell.class) ||
                    targetCell.getCell().getClass().equals(ToxinCell.class))) targetCell.getCell().die();
        }
    }

    @Override
    public String toString() {
        return "T";
    }
}
