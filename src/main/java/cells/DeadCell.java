package cells;

import java.util.concurrent.ThreadLocalRandom;

public class DeadCell extends SimpleCell {

    public DeadCell(SimpleCell parentCell) {
        this.lifetime = (int) (parentCell.getEnergy() > 10 ? parentCell.getEnergy() / 10 : 1);
        this.generation = 0;
        this.energy = parentCell.getEnergy() * ThreadLocalRandom.current().nextDouble(0, 0.5);
    }

    @Override
    public void evolve() {
        if (generation == lifetime) die();
        else generation++;
    }

    @Override
    public void die() {
//        if we don't have any saprofag cells it's too hard to spam toxin
        if (Math.random() < 0.35) {
            fieldCell.setCell(new ToxinCell());
        } else {
            fieldCell.killCell();
        }
    }

    @Override
    public String toString() {
        return "D";
    }
}
