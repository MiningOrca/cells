package cells;

import feild.Field;
import feild.FieldCell;
import model.Coords;
import model.MemoryUnit;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public class SimpleCell {
    protected FieldCell fieldCell;
    protected Field visionField;
    protected int visionRadius;
    protected int lifetime;
    protected int generation;
    protected double energy;
    protected int replicationThreshold = 20;
    protected int moveThreshold;
    protected Memory cellMemory = new Memory();

    public SimpleCell() {
        this.visionRadius = 1;
        this.lifetime = 6;
        this.generation = 0;
    }

    public SimpleCell(SimpleCell parentCell) {
        this.energy = parentCell.getEnergy();
        this.visionRadius = parentCell.getVisionRadius();
        this.lifetime = parentCell.getLifetime();
        this.generation = 0;
    }

    public void evolve() {
        cellMemory.updateMemory();
        feed();
        this.visionField = fieldCell.visionField(visionRadius);
        if (deathConditions()) {
            die();
            return;
        }
        if (moveConditions()) move();
        if (splitConditions()) {
            split();
        }
        generation++;

    }

    protected boolean moveConditions() {
        return energy > moveThreshold * 2;
    }

    protected boolean splitConditions() {
        return generation > 0 && energy >= replicationThreshold;
    }

    protected boolean deathConditions() {
        return generation == lifetime || energy == 0;
    }

    protected void split() {
        calc(visionField, this::idealSplitCalc);
        FieldCell idealSplit = cellMemory.getBestSplit();
        if (idealSplit != null) {
            energy = energy / 2;
            idealSplit.setCell(new SimpleCell(this));
        }
    }

    protected void move() {
//        This Cell cant move at all
//        FieldCell idealMove = calc(visionField, this::idealSplit);
//        if (idealMove != null) {
//            energy = energy - moveThreshold;
//            this.fieldCell.killCell();
//            idealMove.setCell(this);
//        }
    }

    protected void die() {
        fieldCell.setCell(new DeadCell(this));
    }

    protected void feed() {
        energy = energy + 5;
    }

    protected void calc(Field visionField, Consumer<FieldCell> i) {
        for (int x = 0; x < visionField.getFieldCells().length; x++) {
            for (int y = 0; y < visionField.getFieldCells()[x].length; y++) {
                if (visionField.getFieldCells()[x][y] != null)
                    i.accept(visionField.getFieldCell(new Coords(x, y)));
            }
        }
    }

    protected FieldCell getField(Field visionField, BiFunction<FieldCell, FieldCell, FieldCell> i) {
        FieldCell idealCell = null;
        for (int x = 0; x < visionField.getFieldCells().length; x++) {
            for (int y = 0; y < visionField.getFieldCells()[x].length; y++) {
                if (visionField.getFieldCells()[x][y] != null)
                    idealCell = i.apply(visionField.getFieldCell(new Coords(x, y)), idealCell);
            }
        }
        return idealCell;
    }

    protected Coords getField(Field visionField, Function<FieldCell, Coords> i) {
        Coords idealCell = null;
        for (int x = 0; x < visionField.getFieldCells().length; x++) {
            for (int y = 0; y < visionField.getFieldCells()[x].length; y++) {
                if (visionField.getFieldCells()[x][y] != null)
                    idealCell = i.apply(visionField.getFieldCell(new Coords(x, y)));
            }
        }
        return idealCell;
    }

    protected void roundAction(Field actionField, Consumer<FieldCell> action) {
        for (int x = 0; x < actionField.getFieldCells().length; x++) {
            for (int y = 0; y < actionField.getFieldCells()[x].length; y++) {
                if (actionField.getFieldCells()[x][y] != null)
                    action.accept(actionField.getFieldCell(new Coords(x, y)));
            }
        }
    }

    private void idealSplitCalc(FieldCell workingCell) {
        cellMemory.addSplitMemory(workingCell, ThreadLocalRandom.current().nextDouble(0.003));
        if (workingCell.getCell() != null) cellMemory.addSplitMemory(workingCell, -22);
    }

    public void printVisionField() {
        System.out.println("Print vision for " + this.fieldCell.getCoords());
        for (FieldCell[] coord : fieldCell.visionField(visionRadius).getFieldCells()) {
            for (FieldCell cell : coord) {
                System.out.print(cell != null ? cell.getCell() != null ? cell.getCell() + " " : "+ " : "- ");
            }
            System.out.println();
        }
    }

    public int getLifetime() {
        return lifetime;
    }

    public String toString() {
        return generation + "";
    }

    public void setFieldCell(FieldCell fieldCell) {
        this.fieldCell = fieldCell;
    }

    public void setVisionField(Field visionField) {
        this.visionField = visionField;
    }

    public int getVisionRadius() {
        return visionRadius;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    class Memory {
        MemoryUnit lastPosition;
        HashMap<FieldCell, MemoryUnit> memories = new HashMap<>();

        public FieldCell getBestSplit() {
            return memories.entrySet().stream()
                    .max(Comparator.comparing(x -> x.getValue().getSplitWeight())).map(Map.Entry::getKey).orElse(null);
        }

        public FieldCell getBestMove() {
            return memories.entrySet().stream()
                    .max(Comparator.comparing(x -> x.getValue().getMoveWeight())).map(Map.Entry::getKey).orElse(null);
        }

        public void addSplitMemory(FieldCell coords, double value) {
            MemoryUnit unit = memories.get(coords);
            if (unit == null) {
                MemoryUnit memoryUnit = new MemoryUnit(generation);
                memoryUnit.setSplitWeight(value);
                memories.put(coords, memoryUnit);
            } else if (!unit.isPathCell()) {
                memories.get(coords).setSplitWeight(value);
            }
        }

        public void addMoveMemory(FieldCell coords, double value) {
            MemoryUnit unit = memories.get(coords);
            if (unit == null) {
                MemoryUnit memoryUnit = new MemoryUnit(generation);
                memoryUnit.setMoveWeight(value);
                memories.put(coords, memoryUnit);
            } else if (!unit.isPathCell()) {
                memories.get(coords).setMoveWeight(value);
            }
        }

        private void updateMemory() {
            memories.forEach((key, m) -> {
                if (!m.isPathCell() && m.isDieTime(generation)) {
                    memories.remove(key);
                }
            });
        }
    }
}
